import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Main from './main';
import "./index.css";

function App() {
    return (
        <BrowserRouter>
            <div>
                <Routes>
                    <Route path="/" element={<Main/>} />
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
